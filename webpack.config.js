const path = require('path');
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const _PROD = (process.env.NODE_ENV === 'production');

const plugins = [
    new ExtractTextPlugin('styles.css'),
    new webpack.optimize.OccurrenceOrderPlugin(),
    new webpack.NoEmitOnErrorsPlugin()
];

if (_PROD) {
    plugins.push(new webpack.optimize.UglifyJsPlugin());
}

module.exports = {
    entry: [
        'babel-polyfill',
        './src/index'
    ],
    output: {
        path: path.join(__dirname, 'public'),
        filename: 'bundle.js',
        publicPath: '/public/'
    },
    plugins,
    module: {
        rules: [
            {
                test: /\.sass$/,
                loader: ExtractTextPlugin.extract({
                    fallbackLoader: 'style-loader',
                    loader: ['css-loader', 'sass-loader'],
                })
            },
            {
                test: /\.(js)$/,
                include: [
                    path.resolve(__dirname, "src"),
                ],
                loader: 'babel-loader'
            },
        ],
    }
};