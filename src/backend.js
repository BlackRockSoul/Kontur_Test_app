const dataFile = require('./kladr.json');

function findPart(text, callback) {
    text = text.replace(/\\/g, "\\\\");
    let returnArray = [],
        reg = new RegExp(text, 'i'),
        sort_reg = new RegExp('^' + text, 'i');

    for (let i = 0; i < dataFile.length; i++) {
        if (reg.test(dataFile[i].City)) {
            returnArray.push(dataFile[i]);
        }
    }

    returnArray.sort(function (x, y) {
        let xS = x.City.match(sort_reg) || [],
            yS = y.City.match(sort_reg) || [];

        if (xS.length != yS.length) return yS.length - xS.length;

        else return x.City > y.City ? 1 : -1;
    });


    callback({
        array: returnArray.slice(0, 5),
        count: returnArray.length
    });
}

exports.getAutocomplete = function (request, callback) {
    findPart(request, function (data) {
        callback(data);
    });
};