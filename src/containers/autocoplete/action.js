import {
    DATA_RECEIVED,
    DATA_REQUEST,
    DATA_CLEAR,
    DATA_ERROR,
    DATA_LOADER
} from './const';
import fetch from 'isomorphic-fetch';

export function dataSuccess(data) {
    return {
        type: DATA_RECEIVED,
        data: data.array,
        count: data.count
    }
}

export function dataRequest() {
    return {
        type: DATA_REQUEST
    }
}

export function setLoader() {
    return {
        type: DATA_LOADER
    }
}

export function dataClear() {
    return {
        type: DATA_CLEAR
    }
}

export function dataError() {
    return {
        type: DATA_ERROR
    }
}

export function getData(text, url) {
    return (dispatch) => {

        dispatch(dataRequest());

        return fetch('/api/' + url, {
            method: 'post',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({text: text})
        })
            .then(function (response) {
                return response.json()
            })
            .then(response => {
                return dispatch(dataSuccess(response));
            })
            .catch(() => {
                dispatch(dataError());
            })
    }
}