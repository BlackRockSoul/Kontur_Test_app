import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as actionCreators from './action';
import config from '../../config';
import './styles.sass';

class Autocomplete extends Component {
    constructor(props) {
        super(props);
        this.state = {
            dropdownVisible: false,
            error: {
                visible: false,
                text: ''
            },
            hoverElem: 0,
            city: ''
        };
    }

    handleTextChange = (e) => {
        if (this.props.action == 'Loader') return;
        this.setState({
            dropdownVisible: false,
            error: {
                visible: false,
                text: ''
            },
            hoverElem: 0,
            city: ''
        });
        this.props.setCity(this.props.id, '');
        if (e.target.value.trim().length < config.minChars) {
            this.props.setCity(this.props.id, '');
            return this.props.actions.dataClear();
        }
        this.getData(e.target.value);
    };

    getData = (text) => {
        this.props.actions.getData(text.trim(), this.props.url)
            .then(this.setState({
                dropdownVisible: true,
                error: {
                    visible: false,
                    text: '',
                },
                hoverElem: 0
            }));
        setTimeout(() => {
            if (this.props.action == 'Request') {
                this.props.actions.setLoader();
                setTimeout(() => {
                    if (this.props.action == 'Loader') {
                        this.props.actions.dataError();
                    }
                }, 1000)
            }
        }, 500);
    };

    reloadData = (e) => {
        e.preventDefault();
        this.getData(this.refs.input_text.value);
    };

    setCity = (city) => {
        this.refs.input_text.value = city;
        this.setState({
            dropdownVisible: false,
            error: {
                visible: false,
                text: ''
            },
            city: city
        });
        console.log('Значение было выбрано:', city);
        this.props.setCity(this.props.id, city);
    };

    handleKeyPress = (e) => {
        let itemId;
        switch (e.keyCode) {
            case 13: {
                if (!this.refs['itemList' + this.state.hoverElem])
                    return this.setState({
                        dropdownVisible: false,
                        error: {
                            visible: true,
                            text: 'Выберите значение из списка'
                        },
                        city: ''
                    });
                const city = this.refs.input_text.value.trim() ? this.refs['itemList' + this.state.hoverElem].getAttribute('data') : '';
                this.setCity(city);
                break;
            }
            case 38: {
                e.preventDefault();
                itemId = this.state.hoverElem - 1;
                if (this.state.hoverElem > 0)
                    this.setState({
                        hoverElem: itemId
                    });
                break;
            }
            case 40: {
                e.preventDefault();
                itemId = this.state.hoverElem + 1;
                let itemLen = (this.props.data.length > 5) ? 4 : this.props.data.length - 1;
                if (this.state.hoverElem < itemLen)
                    this.setState({
                        hoverElem: itemId
                    });
                break;
            }
            case 27: {
                this.setState({
                    dropdownVisible: false,
                    error: {
                        visible: true,
                        text: 'Выберите значение из списка'
                    }
                })
            }
        }
    };

    cityClickHandler = (e) => {
        const city = e.target.getAttribute('data');
        this.setCity(city);
    };

    onBlur = () => {
        if (this.props.data.length == 1 && !this.state.city) {
            const city = this.refs.input_text.value.trim() ? this.refs['itemList' + this.state.hoverElem].getAttribute('data') : '';
            return this.setCity(city);
        }
        if (!this.state.city) {
            this.setState({
                dropdownVisible: false,
                error: {
                    visible: true,
                    text: 'Выберите значение из списка'
                }
            });
        }
    };

    onFocus = (e) => {
        if (e.target.value.trim().length < config.minChars) {
            this.setState({
                dropdownVisible: false,
                error: {
                    visible: false,
                    text: '',
                },
                hoverElem: 0
            });
            return this.props.actions.dataClear();
        }
        const element = e.target;
        if (this.state.city) {
            return element.setSelectionRange(0, element.value.length);
        }

        this.getData(e.target.value)
    };

    listMouseEnter(i) {
        this.setState({
            hoverElem: i
        })
    }

    render() {
        const {data, count} = this.props,
            inputClass = this.state.error.visible ? 'error ' : '' + this.state.city ? 'selected' : '';

        let foundHandler = null,
            action = null;

        switch (this.props.action) {
            case 'Loader':
                action = <li>
                    <div className="loader"></div>
                    Загрузка
                </li>;
                break;
            case 'Error':
                action = <li>Что-то пошло не так. Проверьте соединение с интернетом и попробуйте еще раз</li>;
                foundHandler = <li onMouseDown={this.reloadData} style={{cursor: 'pointer'}}>Обновить</li>;
                break;
            case 'Received':
                if (data.length && count < 5) {
                    foundHandler = null;
                } else if (data.length) {
                    foundHandler =
                        <li className="shown">
                            Показано {data.length} из {count} найденных инспекций. Уточните запрос, чтобы увидеть
                            остальные
                        </li>
                } else {
                    foundHandler =
                        <li className="notFound">Не найдено</li>
                }
                break;
        }

        return (
            <div className="Autocomplete" style={{display: 'inline-grid'}}>
                <input className={inputClass}
                       type="text"
                       ref="input_text"
                       placeholder="Начните вводить город"
                       onChange={this.handleTextChange}
                       onBlur={this.onBlur}
                       onFocus={this.onFocus}
                       onKeyDown={this.handleKeyPress}
                />

                {this.state.error.visible && <p className="errorText">{this.state.error.text}</p>}

                <ul className={"dropdownList " + ((!this.state.dropdownVisible ||
                ((this.props.action == 'Request' || this.props.action == 'Loader') && action == null)) ? 'none' : '')}>

                    {action}

                    {data.map((obj, i) => {
                        return (
                            <li className={'listItem ' + (this.state.hoverElem == i ? 'hover' : '')}
                                key={'city_' + i}
                                ref={'itemList' + i}
                                onFocus={this.onFocus}
                                onMouseEnter={this.listMouseEnter.bind(this, i)}
                                onMouseDown={this.cityClickHandler}
                                data={obj.City}
                            >{obj.City}</li>
                        )
                    })}

                    {foundHandler}

                </ul>
            </div>
        )
    }
}

Autocomplete.PropTypes = {
    data: PropTypes.array.isRequired,
    count: PropTypes.number.isRequired,
    action: PropTypes.string.isRequired
};

function mapStateToProps(state) {
    return {
        data: state.data,
        count: state.count,
        action: state.action
    }
}

const mapDispatchToProps = (dispatch) => ({
    actions: bindActionCreators(actionCreators, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(Autocomplete);