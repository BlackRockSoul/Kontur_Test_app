export const DATA_RECEIVED = "DATA_RECEIVED";
export const DATA_REQUEST = "DATA_REQUEST";
export const DATA_CLEAR = "DATA_CLEAR";
export const DATA_ERROR = "DATA_ERROR";
export const DATA_LOADER = "DATA_LOADER";