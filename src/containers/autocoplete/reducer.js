const initialState = {
    data: [],
    count: 0,
    action: '',
    err: false
};

import {
    DATA_RECEIVED,
    DATA_REQUEST,
    DATA_CLEAR,
    DATA_ERROR,
    DATA_LOADER
} from './const';

export default function state(state = initialState, payload) {
    switch (payload.type) {
        case DATA_RECEIVED:
            return Object.assign({}, state, {
                data: payload.data,
                count: payload.count,
                action: 'Received',
                err: false
            });
        case DATA_REQUEST:
            return Object.assign({}, state, {
                data: [],
                count: 0,
                action: 'Request',
                err: false
            });
        case DATA_LOADER:
            return Object.assign({}, state, {
                data: [],
                count: 0,
                action: 'Loader',
                err: false
            });
        case DATA_CLEAR:
            return Object.assign({}, state, {
                data: [],
                count: 0,
                action: '',
                err: false
            });
        case DATA_ERROR:
            return Object.assign({}, state, {
                data: [],
                count: 0,
                action: 'Error',
                err: true
            });
        default:
            return state;
    }
}