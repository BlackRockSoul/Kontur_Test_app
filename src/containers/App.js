import React, {Component} from 'react';
import './styles.sass';
import Autocomplete from './autocoplete/Autocomplete';

export default class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            city_1: '',
            city_2: ''
        }
    }

    setData = (name, data) => {
        this.setState({[name]: data});
    };

    render() {
        return (
            <div className="App">
                <div>
                    <p>Город: </p>
                    <Autocomplete id="city_1"
                                  setCity={this.setData}
                                  url="autocomplete"/>
                </div>
                <div>
                    <p>Город: </p>
                    <Autocomplete id="city_2"
                                  setCity={this.setData}
                                  url="autocomplete"/>
                </div>

                <div>
                    {this.state.city_1 && <p>
                        Город 1: {this.state.city_1}
                    </p>}
                    {this.state.city_2 && <p>
                        Город 2: {this.state.city_2}
                    </p>}
                </div>
            </div>
        )
    }
}