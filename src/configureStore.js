import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import rootReducer from './containers/autocoplete/reducer';
import {compose} from 'redux';

export default function configureStore(initialState) {
    const store = compose(
        applyMiddleware(thunk),
    )(createStore)(rootReducer, initialState);

    return store
}