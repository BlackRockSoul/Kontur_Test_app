const express = require('express');
const webpack = require('webpack');
const bodyParser = require('body-parser');
const config = require('./webpack.config');
const backend = require('./src/backend');

const app = express();
const port = 80;

const compiler = webpack(config);
if (process.env.NODE_ENV != 'production') {
    const webpackDevMiddleware = require('webpack-dev-middleware');
    const webpackHotMiddleware = require('webpack-hot-middleware');
    app.use(webpackDevMiddleware(compiler, {noInfo: true, publicPath: config.output.publicPath}));
    app.use(webpackHotMiddleware(compiler));
}
app.use(bodyParser.json());

app.use('/public', express.static('public'));

app.get("/", function (req, res) {
    res.sendFile(__dirname + '/index.html')
});

app.post("/api/autocomplete", function (req, res) {
    backend.getAutocomplete(req.body.text, function (data) {
        res.send(data);
    });
});

app.listen(port, function (error) {
    if (error) {
        console.error(error)
    } else {
        console.info("Listening on port %s.", port);
    }
});